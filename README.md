# Tool to Administrate a 7 Days to Die server. #
* -View Player online/offline
* -View Inventary
* -Chat
* -View Steam Profile

## in Progress ##

* -Whitelist add/remove
* -Kick
* -Ban
* -See edit GamePreferences
* -Save Datas localy for detailed Userinfos last seen online because they also listed if tehy are online

## Requirements: ##

* -Moded Server with Allocs Server fix
* -Java


## How to use: ##
Download the compiled File in Source Folder /target/7daystoddie-v.x.x.x.jar
* Run it by doubleklick or with comand java -jar {downloaded version}.jar 
* Optional Parameters to start 
* host
* port
* pasword
* Usage java -jar {downloaded version}.jar {host} {port} {password}

## Screenshots ##
![7dtda1.PNG](https://bitbucket.org/repo/96raRE/images/2999903657-7dtda1.PNG)
![7dtda2.PNG](https://bitbucket.org/repo/96raRE/images/1984584816-7dtda2.PNG)
![7dtda3.PNG](https://bitbucket.org/repo/96raRE/images/3275459370-7dtda3.PNG)
package oski.sevendayszodie;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.text.DefaultCaret;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import oski.sevendayszodie.conn.ChatTable;
import oski.sevendayszodie.conn.MessageHandler;
import oski.sevendayszodie.conn.MultilineCellRenderer;
import oski.sevendayszodie.conn.player;
import oski.sevendayszodie.conn.playerTable;
import oski.sevendayszodie.conn.sevenDaysToDieConn;

import java.awt.Toolkit;

public class MainWindow {

	private JFrame frmDaysTo;
	private JTextField textFieldHost;
	private JTextField textFieldPort;
	private JTextField textFieldPassword;
	private JTextArea textAreaTelnetLog;
	private JTextField txtCommand;
	private sevenDaysToDieConn tn = null;
	private static JTable table;
	private playerTable tableDataPlOnl = new playerTable();
	private JLabel lblFps;
	private JLabel lblTime;
	private JLabel lblOffline;
	private JLabel labelSN;
	private JLabel lblMaxPl;
	private JLabel lblgammodevalue;
	private JTable chatTable;
	static String host;
	static String port;
	static String password;
	
	
	
	TableModelListener rowAddedScrollToEnd; 
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		if (args.length == 3)
		{
			host = new String(args[0]);
			port = new String(args[1]);
			password = new String(args[2]);
		}
		try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmDaysTo.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		
		rowAddedScrollToEnd = new TableModelListener() {
			
			@Override
		    public void tableChanged(TableModelEvent e) {
		        if (TableModelEvent.INSERT > 0) {
		            SwingUtilities.invokeLater(new Runnable() {
		                @Override
						public void run() {
		                    int viewRow = chatTable.convertRowIndexToView(e.getLastRow());
		                    System.out.println(viewRow);
		                    chatTable.scrollRectToVisible(chatTable.getCellRect(viewRow, 0, true));    
		                }
		            });
		        }
			}
		        
		};
		
		
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		
		frmDaysTo = new JFrame();
		frmDaysTo.setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/oski/images/7dtd.gif")));
		frmDaysTo.setTitle("7 Days to Die Server Admin (Telnet GUI)");
		frmDaysTo.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				if (tn != null)
					tn.close();
			}
		});
		frmDaysTo.getContentPane().setSize(new Dimension(300, 200));
		frmDaysTo.getContentPane().setPreferredSize(new Dimension(300, 200));
		frmDaysTo.setMinimumSize(new Dimension(800, 400));
		frmDaysTo.setBounds(100, 100, 450, 300);
		frmDaysTo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDaysTo.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("-7px:grow"),},
			new RowSpec[] {
				RowSpec.decode("34px"),
				RowSpec.decode("35px"),
				RowSpec.decode("188px:grow"),}));
		
		
		
		
		
		JPanel panel = new JPanel();
		panel.setMinimumSize(new Dimension(300, 10));
		frmDaysTo.getContentPane().add(panel, "1, 1, fill, top");
		
		JLabel lblHost = new JLabel("Host");
		panel.add(lblHost);
		
		textFieldHost = new JTextField((host != null)? host : "");
		panel.add(textFieldHost);
		textFieldHost.setColumns(10);
		
		JLabel lblPort = new JLabel("Port");
		panel.add(lblPort);
		
		textFieldPort = new JTextField((port != null)? port : "");
		panel.add(textFieldPort);
		textFieldPort.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		panel.add(lblPassword);
		
		textFieldPassword = new JPasswordField((password != null)? password : "");
		textFieldPassword.setColumns(10);
		panel.add(textFieldPassword);
		
		JButton btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				tn = new sevenDaysToDieConn();
				MessageHandler mh = new MessageHandler() {
					
					@Override
					public void onMessageReceived(String message) {
						textAreaTelnetLog.append(message);
						
					}
				};
				if(!tn.connect(textFieldHost.getText(), new Integer(textFieldPort.getText())))
				{
					JOptionPane.showMessageDialog(frmDaysTo, "Error to Connect to "+textFieldHost.getText()+":"+textFieldPort.getText(), "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				tn.registerMessageHandler(mh);
				
				Boolean loggedIn = false;
				String pass = textFieldPassword.getText();
				
				loggedIn = tn.login(pass);
				
				while(!loggedIn)
				{
					int ok = 0;
					JPasswordField passwd = new JPasswordField(10);
					JLabel jl = new JLabel("Your Password is incorect.");
					JLabel jl2 = new JLabel("Pleasype type the correct one or Cancel:");
					JPanel jp = new JPanel();
					jp.setLayout(new BoxLayout(jp, BoxLayout.Y_AXIS));
					jp.add(jl);
					jp.add(jl2);
					jp.add(passwd);
					ok = JOptionPane.showConfirmDialog(frmDaysTo, jp, "Wrong Password", JOptionPane.OK_CANCEL_OPTION);
					
					if (ok == 2 || ok == -1)
					{
						tn.close();
						return;
					}
					loggedIn = tn.login(String.valueOf(passwd.getPassword()));
				}

				if(loggedIn)
				{
				
				Thread updateDatas = new Thread("updateDatas"){
					@Override
					@SuppressWarnings("deprecation")
					public void run()
					{
						while(tn.isConnected())
						{
							try {
								if(tn.getGameName() != null)
									labelSN.setText(tn.getGameName());
								if(tn.getMaxPlayers() != null)
									lblMaxPl.setText(tn.getMaxPlayers());
								if(tn.getGameMode() != null);
									lblgammodevalue.setText(tn.getGameMode());
								lblFps.setText(tn.getFps() + " FPS");
								lblTime.setText(tn.getTime());
								lblOffline.setText("online");
								Thread.sleep(3000);
								tn.sendData("lp");
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						lblOffline.setText("offline");
						this.stop();
					}
				};
				
				updateDatas.start();
				table.setModel(tn.getPlayerAllTable());
				chatTable.setModel(tn.getChatTable());
				chatTable.getColumnModel().getColumn(2).setPreferredWidth(600);
				chatTable.setDefaultRenderer(String.class, new MultilineCellRenderer());
				chatTable.getModel().addTableModelListener(rowAddedScrollToEnd);
				//TODO Irgendwie ist das noch nicht das wahre... bekomme keinen Zeilenumbruch zustande chatTable.setRowHeight(50);
				
				
			
			}else
			{
				tn.unregisterMessageHandler(mh);
			}
			}
			
		});
		panel.add(btnConnect);
		
		JPanel panel_3 = new JPanel();
		frmDaysTo.getContentPane().add(panel_3, "1, 2, fill, fill");
		panel_3.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		txtCommand = new JTextField();
		panel_3.add(txtCommand);
		txtCommand.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER)
				{
					System.out.print("Enter pressed");
					if (tn != null)
					{
						tn.sendData(txtCommand.getText());
						txtCommand.setText("");
					}
				}
			}
		});
		txtCommand.setText("Command");
		txtCommand.setColumns(10);
		
		lblOffline = new JLabel("offline");
		panel_3.add(lblOffline);
		
		Component horizontalStrut = Box.createHorizontalStrut(10);
		panel_3.add(horizontalStrut);
		
		lblFps = new JLabel("0 FPS");
		panel_3.add(lblFps);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(10);
		panel_3.add(horizontalStrut_1);
		
		lblTime = new JLabel("--");
		panel_3.add(lblTime);
		
		JLabel lblServername = new JLabel("ServerName;");
		panel_3.add(lblServername);
		
		labelSN = new JLabel("--");
		panel_3.add(labelSN);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(10);
		panel_3.add(horizontalStrut_2);
		
		JLabel lblMaxplayers = new JLabel("MaxPlayers:");
		panel_3.add(lblMaxplayers);
		
		lblMaxPl = new JLabel("--");
		panel_3.add(lblMaxPl);
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(10);
		panel_3.add(horizontalStrut_3);
		
		JLabel lblGamemode = new JLabel("GameMode:");
		panel_3.add(lblGamemode);
		
		lblgammodevalue = new JLabel("--");
		panel_3.add(lblgammodevalue);
		
		JPanel panel_1 = new JPanel();
		panel_1.setMinimumSize(new Dimension(600, 300));
		frmDaysTo.getContentPane().add(panel_1, "1, 3, fill, top");
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.X_AXIS));
		
		JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
		panel_1.add(tabbedPane);
		
		JScrollPane scrollPane = new JScrollPane();
		tabbedPane.addTab("ServerLogs", null, scrollPane, null);
		tabbedPane.setEnabledAt(0, true);
		scrollPane.setAutoscrolls(true);
		
		textAreaTelnetLog = new JTextArea();
		textAreaTelnetLog.setRows(100);
		textAreaTelnetLog.setMinimumSize(new Dimension(0, 400));
		textAreaTelnetLog.setWrapStyleWord(true);
		textAreaTelnetLog.setEditable(false);
		textAreaTelnetLog.setColumns(45);
		DefaultCaret dc = (DefaultCaret) textAreaTelnetLog.getCaret();
		scrollPane.setViewportView(textAreaTelnetLog);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Player All", null, panel_2, null);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_2.add(scrollPane_1);
		
		table = new JTable();
		
		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(table, popupMenu);
		
		JMenuItem mntmShowDetails = new JMenuItem("Show Details");
		mntmShowDetails.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				
						Integer row = table.convertRowIndexToModel(table.getSelectedRow());
						playerTable tableModel = (playerTable) table.getModel();
						player pl = tableModel.data.get(row);
						/*
						JFrame infoPlayer = new JFrame("Player Info");
						infoPlayer.setSize(300, 500);
						infoPlayer.setLocation(200, 200);
						infoPlayer.getContentPane().setLayout(new FormLayout(new ColumnSpec[] {
								ColumnSpec.decode("100px:grow"),
								ColumnSpec.decode("100px:grow")
						}, new RowSpec[] {
							RowSpec.decode("20px"),
							RowSpec.decode("20px"),
							RowSpec.decode("20px"),
							RowSpec.decode("20px"),
							RowSpec.decode("25px"),
							RowSpec.decode("20px"),
							RowSpec.decode("20px"),
							RowSpec.decode("20px")
						}));
						JLabel nameLabel = new JLabel("Name:");
						infoPlayer.getContentPane().add(nameLabel, "1, 1, default, default");
						
						JLabel nameValue = new JLabel(pl.name);
						infoPlayer.getContentPane().add(nameValue, "2, 1, default, default");
						
						JLabel serveridLabel = new JLabel("Server ID:");
						infoPlayer.getContentPane().add(serveridLabel, "1, 2, default, default");
						
						JLabel serveridValue = new JLabel(pl.serverid.toString());
						infoPlayer.getContentPane().add(serveridValue, "2, 2, default, default");
						
						JLabel deathsLabel = new JLabel("Deaths:");
						infoPlayer.getContentPane().add(deathsLabel, "1, 3, default, default");
						
						JLabel deathsValue = new JLabel((pl.deaths == null)? "n/a" : pl.deaths.toString());
						infoPlayer.getContentPane().add(deathsValue, "2, 3, default, default");
						
						JLabel zombiesLabel = new JLabel("Zomies killed:");
						infoPlayer.getContentPane().add(zombiesLabel, "1, 4, default, default");
						
						JLabel zombiesValue = new JLabel((pl.zombies == null)? "n/a" : pl.zombies.toString());
						infoPlayer.getContentPane().add(zombiesValue, "2, 4, default, default");
						
						JLabel playersLabel = new JLabel("Players killed:");
						infoPlayer.getContentPane().add(playersLabel, "1, 5, default, default");
						
						JLabel playersValue = new JLabel((pl.players == null)? "n/a": pl.players.toString());
						infoPlayer.getContentPane().add(playersValue, "2, 5, default, default");
						
						JLabel healthLabel = new JLabel("Max. Health:");
						infoPlayer.getContentPane().add(healthLabel, "1, 6, default, default");
						
						JLabel healthValue = new JLabel((pl.health == null)? "n/a": pl.health.toString());
						infoPlayer.getContentPane().add(healthValue, "2, 6, default, default");
						
						JLabel scoreLabel = new JLabel("Score:");
						infoPlayer.getContentPane().add(scoreLabel, "1, 7, default, default");
						
						JLabel scoreValue = new JLabel((pl.score == null)? "n/a": pl.score.toString());
						infoPlayer.getContentPane().add(scoreValue, "2, 7, default, default");
						
						
						infoPlayer.setVisible(true);
					
				*/
				
				
				PlayerDetailsWindow pdw = new  PlayerDetailsWindow(pl, tn);
				
				
			}
		});
		popupMenu.add(mntmShowDetails);
		
		JMenuItem mntmShowSteamAccoung = new JMenuItem("Show Steam Accoung");
		mntmShowSteamAccoung.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				
				int row = table.getSelectedRow();
				
				String steamID = table.getValueAt(row, 2).toString();
				
				if (Desktop.isDesktopSupported())
				{
					try {
						Desktop.getDesktop().browse(new URL("http://steamcommunity.com/profiles/" + steamID + "/").toURI());
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (URISyntaxException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
		});
		popupMenu.add(mntmShowSteamAccoung);
		
		JMenuItem mntmAddToWhitelist = new JMenuItem("Add to Whitelist");
		popupMenu.add(mntmAddToWhitelist);
		
		JMenuItem mntmRemoveFromWhitelist = new JMenuItem("Remove from Whitelist");
		popupMenu.add(mntmRemoveFromWhitelist);
		
		JMenuItem mntmKickPlayer = new JMenuItem("Kick Player");
		popupMenu.add(mntmKickPlayer);
		
		JMenuItem mntmBanPlayer = new JMenuItem("Ban Player");
		popupMenu.add(mntmBanPlayer);
		table.setAutoCreateRowSorter(true);
		scrollPane_1.setViewportView(table);
		table.setModel(this.tableDataPlOnl);
		
		table.addMouseListener( new MouseAdapter()
		{
			@Override
			public void mousePressed( MouseEvent e )
			{
				// Left mouse click
				if ( SwingUtilities.isLeftMouseButton( e ) )
				{
					// Do something
				}
				// Right mouse click
				else if ( SwingUtilities.isRightMouseButton( e ) )
				{
					// get the coordinates of the mouse click
					Point p = e.getPoint();
		 
					// get the row index that contains that coordinate
					int rowNumber = table.rowAtPoint( p );
		 
					// Get the ListSelectionModel of the JTable
					ListSelectionModel model = table.getSelectionModel();
		 
					// set the selected interval of rows. Using the "rowNumber"
					// variable for the beginning and end selects only that one row.
					model.setSelectionInterval( rowNumber, rowNumber );
				}
			}
		});
		
		tabbedPane.setEnabledAt(1, true);
		
		JPanel panel_4 = new JPanel();
		tabbedPane.addTab("Chat", null, panel_4, null);
		tabbedPane.setEnabledAt(2, true);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));
		
		JScrollPane scrollPane_2 = new JScrollPane();
		panel_4.add(scrollPane_2);
		
		chatTable = new JTable();
		chatTable.setModel(new ChatTable());
		chatTable.getColumnModel().getColumn(2).setPreferredWidth(600);
		chatTable.setDefaultRenderer(String.class, new MultilineCellRenderer());
		
		chatTable.getModel().addTableModelListener(rowAddedScrollToEnd);
		scrollPane_2.setViewportView(chatTable);
		
		JTextField chatTextField = new JTextField();
		chatTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
				{
					String message = chatTextField.getText();
					if (tn != null && tn.isConnected())
					{
						tn.sendData("say \"" + message + "\"");
						chatTextField.setText("");
					}
				}
			}
		});
		panel_4.add(chatTextField);
		dc.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
	}

	public JTextArea getTextAreaTelnetLog() {
		return textAreaTelnetLog;
	}
	public JLabel getLblFps() {
		return lblFps;
	}
	public JLabel getLblTime() {
		return lblTime;
	}
	public JLabel getLblOffline() {
		return lblOffline;
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					
					
					showMenu(e);
					
					
					
				}
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	public JTable getTable() {
		return table;
	}
	public JLabel getLabelSN() {
		return labelSN;
	}
	public JLabel getLblMaxPl() {
		return lblMaxPl;
	}
	public JLabel getLblgammodevalue() {
		return lblgammodevalue;
	}
}

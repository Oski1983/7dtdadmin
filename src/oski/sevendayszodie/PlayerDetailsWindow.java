package oski.sevendayszodie;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.ImageIcon;

import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.JPanel;
import java.awt.Image;
import javax.swing.JLabel;
import net.miginfocom.swing.MigLayout;
import oski.sevendayszodie.conn.player;
import oski.sevendayszodie.conn.playerInventaryAvailable;
import oski.sevendayszodie.conn.sevenDaysToDieConn;

import javax.swing.border.LineBorder;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.HashMap;
import java.awt.Dimension;
import javax.swing.Box;

public class PlayerDetailsWindow {

	private JFrame frame;
	static player player;
	static sevenDaysToDieConn conn;
	private JLabel bpSlot1;
	private JLabel bpSlot2;
	private JLabel bpSlot3;
	private JLabel bpSlot4;
	private JLabel bpSlot5;
	private JLabel bpSlot6;
	private JLabel bpSlot7;
	private JLabel bpSlot8;
	private JLabel bpSlot9;
	private JLabel bpSlot10;
	private JLabel bpSlot11;
	private JLabel bpSlot12;
	private JLabel bpSlot13;
	private JLabel bpSlot14;
	private JLabel bpSlot15;
	private JLabel bpSlot16;
	private JLabel bpSlot17;
	private JLabel bpSlot18;
	private JLabel bpSlot19;
	private JLabel bpSlot20;
	private JLabel lblValueName;
	private JLabel lblSteamIDValue;
	
	private Thread updater;

	/**
	 * Launch the application.
	 */
	public static void main(player player, sevenDaysToDieConn tn) {
		try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PlayerDetailsWindow window = new PlayerDetailsWindow(player, tn);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @param pl 
	 * @param tn 
	 */
	public PlayerDetailsWindow(oski.sevendayszodie.conn.player pl, sevenDaysToDieConn tn) {
		player = pl;
		conn = tn;
		try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		initialize();
		frame.setVisible(true);
		update();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(PlayerDetailsWindow.class.getResource("/oski/images/7dtd.gif")));
		frame.setResizable(false);
		frame.setBounds(100, 100, 780, 706);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setTitle("Details of Player "+player.name);
		frame.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				updater.stop();
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panelPlayerDetails = new JPanel();
		frame.getContentPane().add(panelPlayerDetails, BorderLayout.WEST);
		panelPlayerDetails.setLayout(new MigLayout("", "[][75px]", "[40.00px][38.00px]"));
		
		JLabel lblLabelName = new JLabel("Player Name:");
		panelPlayerDetails.add(lblLabelName, "cell 0 0,grow");
		
		lblValueName = new JLabel("n/a");
		panelPlayerDetails.add(lblValueName, "cell 1 0,grow");
		
		JLabel lblLabelSteamID = new JLabel("SteamID:\r\n");
		panelPlayerDetails.add(lblLabelSteamID, "cell 0 1");
		
		lblSteamIDValue = new JLabel("n/a");
		panelPlayerDetails.add(lblSteamIDValue, "cell 1 1");
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new MigLayout("", "[600.00,grow]", "[135.00][][344.00,grow,shrink 50]"));
		
		JPanel panelPlayerPanel = new JPanel();
		panel.add(panelPlayerPanel, "cell 0 0,grow");
		
		beltSlot1 = new JLabel();
		beltSlot1.setPreferredSize(new Dimension(60, 60));
		beltSlot1.setName("beltSlot1");
		beltSlot1.setBorder(new LineBorder(null, 2, true));
		panelPlayerPanel.add(beltSlot1);
		
		beltSlot2 = new JLabel();
		beltSlot2.setPreferredSize(new Dimension(60, 60));
		beltSlot2.setName("beltSlot2");
		beltSlot2.setBorder(new LineBorder(null, 2, true));
		panelPlayerPanel.add(beltSlot2);
		
		beltSlot3 = new JLabel();
		beltSlot3.setPreferredSize(new Dimension(60, 60));
		beltSlot3.setName("beltSlot3");
		beltSlot3.setBorder(new LineBorder(null, 2, true));
		panelPlayerPanel.add(beltSlot3);
		
		beltSlot4 = new JLabel();
		beltSlot4.setPreferredSize(new Dimension(60, 60));
		beltSlot4.setName("beltSlot4");
		beltSlot4.setBorder(new LineBorder(null, 2, true));
		panelPlayerPanel.add(beltSlot4);
		
		beltSlot5 = new JLabel();
		beltSlot5.setPreferredSize(new Dimension(60, 60));
		beltSlot5.setName("beltSlot5");
		beltSlot5.setBorder(new LineBorder(null, 2, true));
		panelPlayerPanel.add(beltSlot5);
		
		beltSlot6 = new JLabel();
		beltSlot6.setPreferredSize(new Dimension(60, 60));
		beltSlot6.setName("beltSlot6");
		beltSlot6.setBorder(new LineBorder(null, 2, true));
		panelPlayerPanel.add(beltSlot6);
		
		beltSlot7 = new JLabel();
		beltSlot7.setPreferredSize(new Dimension(60, 60));
		beltSlot7.setName("beltSlot7");
		beltSlot7.setBorder(new LineBorder(null, 2, true));
		panelPlayerPanel.add(beltSlot7);
		
		beltSlot8 = new JLabel();
		beltSlot8.setPreferredSize(new Dimension(60, 60));
		beltSlot8.setName("beltSlot8");
		beltSlot8.setBorder(new LineBorder(null, 2, true));
		panelPlayerPanel.add(beltSlot8);
		
		horizontalStrut_1 = Box.createHorizontalStrut(150);
		panel.add(horizontalStrut_1, "flowx,cell 0 1");
		
		lblNewLabel = new JLabel("BagPack");
		panel.add(lblNewLabel, "cell 0 1,aligny baseline");
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, "cell 0 2,grow");
		panel_1.setLayout(new MigLayout("", "[334.00,grow][grow]", "[grow]"));
		
		JPanel panelBagPack = new JPanel();
		panelBagPack.setBorder(new LineBorder(null, 2, true));
		panel_1.add(panelBagPack, "cell 0 0,grow");
		panelBagPack.setLayout(new MigLayout("", "[grow][grow][grow][grow][grow]", "[grow][grow][grow][grow][grow][grow][]"));
		
		bpSlot1 = new JLabel();
		bpSlot1.setPreferredSize(new Dimension(60, 60));
		bpSlot1.setName("bpSlot1");
		bpSlot1.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot1, "cell 0 0,grow");
		
		bpSlot2 = new JLabel();
		bpSlot2.setPreferredSize(new Dimension(60, 60));
		bpSlot2.setName("bpSlot2");
		bpSlot2.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot2, "cell 1 0,grow");
		
		bpSlot3 = new JLabel();
		bpSlot3.setPreferredSize(new Dimension(60, 60));
		bpSlot3.setName("bpSlot3");
		bpSlot3.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot3, "cell 2 0,grow");
		
		bpSlot4 = new JLabel();
		bpSlot4.setPreferredSize(new Dimension(60, 60));
		bpSlot4.setName("bpSlot4");
		bpSlot4.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot4, "cell 3 0,grow");
		
		bpSlot5 = new JLabel();
		bpSlot5.setPreferredSize(new Dimension(60, 60));
		bpSlot5.setName("bpSlot5");
		bpSlot5.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot5, "cell 4 0,grow");
		
		bpSlot6 = new JLabel();
		bpSlot6.setPreferredSize(new Dimension(60, 60));
		bpSlot6.setName("bpSlot6");
		bpSlot6.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot6, "cell 0 1,grow");
		
		bpSlot7 = new JLabel();
		bpSlot7.setPreferredSize(new Dimension(60, 60));
		bpSlot7.setName("bpSlot7");
		bpSlot7.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot7, "cell 1 1,grow");
		
		bpSlot8 = new JLabel();
		bpSlot8.setPreferredSize(new Dimension(60, 60));
		bpSlot8.setName("bpSlot8");
		bpSlot8.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot8, "cell 2 1,grow");
		
		bpSlot9 = new JLabel();
		bpSlot9.setPreferredSize(new Dimension(60, 60));
		bpSlot9.setName("bpSlot9");
		bpSlot9.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot9, "cell 3 1,grow");
		
		bpSlot10 = new JLabel();
		bpSlot10.setPreferredSize(new Dimension(60, 60));
		bpSlot10.setName("bpSlot10");
		bpSlot10.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot10, "cell 4 1,grow");
		
		bpSlot11 = new JLabel();
		bpSlot11.setPreferredSize(new Dimension(60, 60));
		bpSlot11.setName("bpSlot11");
		bpSlot11.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot11, "cell 0 2,grow");
		
		bpSlot12 = new JLabel();
		bpSlot12.setPreferredSize(new Dimension(60, 60));
		bpSlot12.setName("bpSlot12");
		bpSlot12.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot12, "cell 1 2,grow");
		
		bpSlot13 = new JLabel();
		bpSlot13.setPreferredSize(new Dimension(60, 60));
		bpSlot13.setName("bpSlot13");
		bpSlot13.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot13, "cell 2 2,grow");
		
		bpSlot14 = new JLabel();
		bpSlot14.setPreferredSize(new Dimension(60, 60));
		bpSlot14.setName("bpSlot14");
		bpSlot14.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot14, "cell 3 2,grow");
		
		bpSlot15 = new JLabel();
		bpSlot15.setPreferredSize(new Dimension(60, 60));
		bpSlot15.setName("bpSlot15");
		bpSlot15.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot15, "cell 4 2,grow");
		
		bpSlot16 = new JLabel();
		bpSlot16.setPreferredSize(new Dimension(60, 60));
		bpSlot16.setName("bpSlot16");
		bpSlot16.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot16, "cell 0 3,grow");
		
		bpSlot17 = new JLabel();
		bpSlot17.setPreferredSize(new Dimension(60, 60));
		bpSlot17.setName("bpSlot17");
		bpSlot17.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot17, "cell 1 3,grow");
		
		bpSlot18 = new JLabel();
		bpSlot18.setPreferredSize(new Dimension(60, 60));
		bpSlot18.setName("bpSlot18");
		bpSlot18.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot18, "cell 2 3,grow");
		
		bpSlot19 = new JLabel();
		bpSlot19.setPreferredSize(new Dimension(60, 60));
		bpSlot19.setName("bpSlot19");
		bpSlot19.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot19, "cell 3 3,grow");
		
		bpSlot20 = new JLabel();
		bpSlot20.setPreferredSize(new Dimension(60, 60));
		bpSlot20.setName("bpSlot20");
		bpSlot20.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot20, "cell 4 3,grow");
		
		bpSlot21 = new JLabel();
		bpSlot21.setPreferredSize(new Dimension(60, 60));
		bpSlot21.setName("bpSlot21");
		bpSlot21.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot21, "cell 0 4, grow");
		
		bpSlot22 = new JLabel();
		bpSlot22.setPreferredSize(new Dimension(60, 60));
		bpSlot22.setName("bpSlot22");
		bpSlot22.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot22, "cell 1 4, grow");
		
		bpSlot23 = new JLabel();
		bpSlot23.setPreferredSize(new Dimension(60, 60));
		bpSlot23.setName("bpSlot23");
		bpSlot23.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot23, "cell 2 4, grow");
		
		bpSlot24 = new JLabel();
		bpSlot24.setPreferredSize(new Dimension(60, 60));
		bpSlot24.setName("bpSlot24");
		bpSlot24.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot24, "cell 3 4, grow");
		
		bpSlot25 = new JLabel();
		bpSlot25.setPreferredSize(new Dimension(60, 60));
		bpSlot25.setName("bpSlot25");
		bpSlot25.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot25, "cell 4 4, grow");
		
		bpSlot26 = new JLabel();
		bpSlot26.setPreferredSize(new Dimension(60, 60));
		bpSlot26.setName("bpSlot26");
		bpSlot26.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot26, "cell 0 5, grow");
		
		bpSlot27 = new JLabel();
		bpSlot27.setPreferredSize(new Dimension(60, 60));
		bpSlot27.setName("bpSlot27");
		bpSlot27.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot27, "cell 1 5, grow");
		
		bpSlot28 = new JLabel();
		bpSlot28.setPreferredSize(new Dimension(60, 60));
		bpSlot28.setName("bpSlot28");
		bpSlot28.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot28, "cell 2 5, grow");
		
		bpSlot29 = new JLabel();
		bpSlot29.setPreferredSize(new Dimension(60, 60));
		bpSlot29.setName("bpSlot29");
		bpSlot29.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot29, "cell 3 5, grow");
		
		bpSlot30 = new JLabel();
		bpSlot30.setPreferredSize(new Dimension(60, 60));
		bpSlot30.setName("bpSlot30");
		bpSlot30.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot30, "cell 4 5, grow");
		
		bpSlot31 = new JLabel();
		bpSlot31.setPreferredSize(new Dimension(60, 60));
		bpSlot31.setName("bpSlot31");
		bpSlot31.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot31, "cell 0 6");
		
		bpSlot32 = new JLabel();
		bpSlot32.setPreferredSize(new Dimension(60, 60));
		bpSlot32.setName("bpSlot32");
		bpSlot32.setBorder(new LineBorder(null, 2, true));
		panelBagPack.add(bpSlot32, "cell 4 6");
		
		
		
		JPanel panelEquip = new JPanel();
		panel_1.add(panelEquip, "cell 1 0,grow");
		panelEquip.setLayout(new MigLayout("", "[][][grow]", "[][][][]"));
		
		invSlot1 = new JLabel();
		invSlot1.setPreferredSize(new Dimension(65, 65));
		invSlot1.setName("invSlot1");
		invSlot1.setBorder(new LineBorder(null, 2, true));
		panelEquip.add(invSlot1, "cell 0 0");
		
		invSlot2 = new JLabel();
		invSlot2.setPreferredSize(new Dimension(65, 65));
		invSlot2.setName("invSlot2");
		invSlot2.setBorder(new LineBorder(null, 2, true));
		panelEquip.add(invSlot2, "cell 1 0");
		
		invSlot3 = new JLabel();
		invSlot3.setPreferredSize(new Dimension(65, 65));
		invSlot3.setName("invSlot3");
		invSlot3.setBorder(new LineBorder(null, 2, true));
		panelEquip.add(invSlot3, "cell 2 0");
		
		invSlot4 = new JLabel();
		invSlot4.setPreferredSize(new Dimension(65, 65));
		invSlot4.setName("invSlot4");
		invSlot4.setBorder(new LineBorder(null, 2, true));
		panelEquip.add(invSlot4, "cell 0 1");
		
		invSlot5 = new JLabel();
		invSlot5.setPreferredSize(new Dimension(65, 65));
		invSlot5.setName("invSlot5");
		invSlot5.setBorder(new LineBorder(null, 2, true));
		panelEquip.add(invSlot5, "cell 1 1");
		
		invSlot6 = new JLabel();
		invSlot6.setPreferredSize(new Dimension(65, 65));
		invSlot6.setName("invSlot6");
		invSlot6.setBorder(new LineBorder(null, 2, true));
		panelEquip.add(invSlot6, "cell 2 1");
		
		invSlot7 = new JLabel();
		invSlot7.setPreferredSize(new Dimension(65, 65));
		invSlot7.setName("invSlot7");
		invSlot7.setBorder(new LineBorder(null, 2, true));
		panelEquip.add(invSlot7, "cell 0 2");
		
		invSlot8 = new JLabel();
		invSlot8.setPreferredSize(new Dimension(65, 65));
		invSlot8.setName("invSlot8");
		invSlot8.setBorder(new LineBorder(null, 2, true));
		panelEquip.add(invSlot8, "cell 1 2");
		
		invSlot9 = new JLabel();
		invSlot9.setPreferredSize(new Dimension(65, 65));
		invSlot9.setName("invSlot9");
		invSlot9.setBorder(new LineBorder(null, 2, true));
		panelEquip.add(invSlot9, "cell 2 2");
		
		invSlot10 = new JLabel();
		invSlot10.setPreferredSize(new Dimension(65, 65));
		invSlot10.setName("invSlot10");
		invSlot10.setBorder(new LineBorder(null, 2, true));
		panelEquip.add(invSlot10, "cell 0 3");
		
		invSlot11 = new JLabel();
		invSlot11.setPreferredSize(new Dimension(65, 65));
		invSlot11.setName("invSlot11");
		invSlot11.setBorder(new LineBorder(null, 2, true));
		panelEquip.add(invSlot11, "cell 2 3");
		
		horizontalStrut = Box.createHorizontalStrut(230);
		panel.add(horizontalStrut, "cell 0 1");
		
		lblNewLabel_1 = new JLabel("Inventary");
		panel.add(lblNewLabel_1, "cell 0 1");
		
		//maping BagPack
		
		for(Component child : panelBagPack.getComponents())
		{
			if (child instanceof JLabel && child.getName().matches("bp.*"))
			{
				bagBackLabels.put(child.getName(), (JLabel)child);
			}
		}
		
		for (Component child : panelPlayerPanel.getComponents())
		{
			if (child instanceof JLabel && child.getName().matches("belt.*"))
			{
				beltLabels.put(child.getName(), (JLabel)child);
			}
			
		}
		
		for (Component child : panelEquip.getComponents())
		{
			if (child instanceof JLabel && child.getName().matches("inv.*"))
			{
				invLabels.put(child.getName(), (JLabel)child);
			}
			
		}
		
		
		
		//add Thread for update
		updater = new Thread("Update " + player.name)
				{
			public void run()
			{
				boolean firstRun = true;
				while(true)
				{
					if (firstRun || player.isOnline)
					{
						conn.getInventary(player);
						try {
							if (firstRun)
							{
								Thread.sleep(2500);
								firstRun = false;
							}
							else
								Thread.sleep(5000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				}
			}
				};
				updater.start();
				
				playerInventaryAvailable pia = new playerInventaryAvailable() {
					
					@Override
					public void onPlayerInventaryAvailable(player pl) {
						// TODO Auto-generated method stub
						System.out.println("Player Inventary now available");
						update();
						
					}
				};
				
				player.addMessageHandler(pia);
				
				

				frame.setLocationRelativeTo(null);		
		
	}
	
	public void update()
	{
		for(int i = 0; i < player.bagPack.length; i++)
		{
			JLabel obj = null;
			ImageIcon icon = null;
			if (player.bagPack[i][0] != null) {
				icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/oski/images/" + player.bagPack[i][0] + ".png")));
				obj = bagBackLabels.get("bpSlot" + (i+1));
				if (icon != null && obj != null) {
					Image img = icon.getImage().getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
					
					obj.setToolTipText(player.bagPack[i][0]);
					obj.setIcon(new ImageIcon(img));
					obj.setHorizontalTextPosition(JLabel.CENTER);
					obj.setVerticalAlignment(JLabel.CENTER);
					String text = "<html>" + player.bagPack[i][1] + "X";
					if (player.bagPack[i][2] != null)
						text += "<br />(" + player.bagPack[i][2] + ")";
					text += "</html>";
					obj.setText(text);
					
					
					obj.repaint();
				} 
			}
			else
			{
				obj = bagBackLabels.get("bpSlot" + (i+1));
				if (obj != null && obj.getClass() == JLabel.class) {
					obj.setIcon(null);
					obj.setText(null);
					obj.repaint();
				} 
			}
		}
		
		for(int i = 0; i < player.belt.length; i++)
		{
			JLabel obj = null;
			ImageIcon icon = null;
			if (player.belt[i][0] != null) {
				icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/oski/images/" + player.belt[i][0] + ".png")));
				obj = beltLabels.get("beltSlot" + (i+1));
				if (icon != null && obj != null) {
					Image img = icon.getImage().getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
					
					obj.setToolTipText(player.belt[i][0]);
					obj.setIcon(new ImageIcon(img));
					obj.setHorizontalTextPosition(JLabel.CENTER);
					obj.setVerticalAlignment(JLabel.CENTER);
					String text = "<html>" + player.belt[i][1] + "X";
					if (player.belt[i][2] != null)
						text += "<br />(" + player.belt[i][2] + ")";
					text += "</html>";
					obj.setText(text);
					obj.repaint();
				} 
			}
			else
			{
				obj = beltLabels.get("beltSlot" + (i+1));
				if (obj != null && obj.getClass() == JLabel.class) {
					obj.setIcon(null);
					obj.setText(null);
					obj.repaint();
				} 
			}
		}
		
		for(int i = 0; i < player.equpment.length; i++)
		{
			JLabel obj = null;
			ImageIcon icon = null;
			if (player.equpment[i][0] != null) {
				System.out.println(player.equpment[i][0]);
				icon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/oski/images/" + player.equpment[i][0] + ".png")));
				obj = invLabels.get("invSlot" + (i+1));
				if (icon != null && obj != null) {
					Image img = icon.getImage().getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH);
					
					obj.setToolTipText(player.equpment[i][0]);
					obj.setIcon(new ImageIcon(img));
					obj.setHorizontalTextPosition(JLabel.CENTER);
					obj.setVerticalAlignment(JLabel.CENTER);
					obj.setText("("+ player.equpment[i][1] + ")");
					obj.repaint();
				} 
			}
			else
			{
				obj = invLabels.get("invSlot" + (i+1));
				if (obj != null && obj.getClass() == JLabel.class) {
					obj.setIcon(null);
					obj.setText(null);
					obj.repaint();
				} 
			}
		}
		
		
		
		
		
		lblValueName.setText(player.name);
		lblSteamIDValue.setText(player.uid);
	}
	
	public HashMap<String, JLabel> bagBackLabels = new HashMap<String, JLabel>();
	public HashMap<String, JLabel> beltLabels = new HashMap<String, JLabel>();
	public HashMap<String, JLabel> invLabels = new HashMap<String, JLabel>();
	private JLabel bpSlot21;
	private JLabel bpSlot22;
	private JLabel bpSlot23;
	private JLabel bpSlot24;
	private JLabel bpSlot25;
	private JLabel bpSlot26;
	private JLabel bpSlot27;
	private JLabel bpSlot28;
	private JLabel bpSlot29;
	private JLabel bpSlot30;
	private JLabel beltSlot1;
	private JLabel beltSlot2;
	private JLabel beltSlot3;
	private JLabel beltSlot5;
	private JLabel beltSlot6;
	private JLabel beltSlot8;
	private JLabel beltSlot4;
	private JLabel beltSlot7;
	private JLabel invSlot1;
	private JLabel invSlot2;
	private JLabel invSlot4;
	private JLabel invSlot5;
	private JLabel invSlot7;
	private JLabel invSlot8;
	private JLabel invSlot10;
	private JLabel invSlot3;
	private JLabel invSlot6;
	private JLabel invSlot9;
	private JLabel invSlot11;
	private JLabel bpSlot31;
	private JLabel bpSlot32;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private Component horizontalStrut;
	private Component horizontalStrut_1;

	public JLabel getBpSlot1() {
		return bpSlot1;
	}
	public JLabel getBpSlot2() {
		return bpSlot2;
	}
	public JLabel getBpSlot3() {
		return bpSlot3;
	}
	public JLabel getBpSlot4() {
		return bpSlot4;
	}
	public JLabel getBpSlot5() {
		return bpSlot5;
	}
	public JLabel getBpSlot6() {
		return bpSlot6;
	}
	public JLabel getBpSlot7() {
		return bpSlot7;
	}
	public JLabel getBpSlot8() {
		return bpSlot8;
	}
	public JLabel getBpSlot9() {
		return bpSlot9;
	}
	public JLabel getBpSlot10() {
		return bpSlot10;
	}
	public JLabel getBpSlot11() {
		return bpSlot11;
	}
	public JLabel getBpSlot12() {
		return bpSlot12;
	}
	public JLabel getBpSlot13() {
		return bpSlot13;
	}
	public JLabel getBpSlot14() {
		return bpSlot14;
	}
	public JLabel getBpSlot15() {
		return bpSlot15;
	}
	public JLabel getBpSlot16() {
		return bpSlot16;
	}
	public JLabel getBpSlot17() {
		return bpSlot17;
	}
	public JLabel getBpSlot18() {
		return bpSlot18;
	}
	public JLabel getBpSlot19() {
		return bpSlot19;
	}
	public JLabel getBpSlot20() {
		return bpSlot20;
	}
	public JLabel getLblValueName() {
		return lblValueName;
	}
	public JLabel getLblSteamIDValue() {
		return lblSteamIDValue;
	}
}

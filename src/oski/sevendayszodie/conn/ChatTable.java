package oski.sevendayszodie.conn;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class ChatTable extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<String[]> data;
	
	String[] columns = {
		"Time",
		"Sender",
		"Message"
	};
	


	public ChatTable() {
		// TODO Auto-generated constructor stub
		data = new ArrayList<String[]>(); 
		data.add(new String[]{
			new Time(System.currentTimeMillis()).toString(),
			"System",
			"Chat started! Ja aber wir wollen ja jetzt mal einen etwas oder besser einen viel zu langen Text erzeugen damit wir auch mal sehen k�nnen ob der Zeilenumbruch so funktioniert wie wir uns das vorstellen."
		});
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return this.columns.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return this.data.size();
	}
	
	@Override
	public String getColumnName(int col)
	{
		return this.columns[col];
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return this.data.get(arg0)[arg1];
	}
	
	public Boolean addRow(String time, String sender, String message)
	{
		this.data.add(new String[]{
				time,
				sender,
				message
		});
		this.fireTableRowsInserted((data.size() -1), (data.size() -1));
		return true;
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		// TODO Auto-generated method stub
		return String.class;
	}

}

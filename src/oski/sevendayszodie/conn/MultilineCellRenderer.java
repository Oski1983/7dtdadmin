package oski.sevendayszodie.conn;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellRenderer;

public class MultilineCellRenderer extends JTextArea implements TableCellRenderer
{
	  /**
	     *
	     */
	    
	
	static int lines = 1;
	static int rowWidth = 50;
	Font font = getFont();
	
	

	@Override
	public Component getTableCellRendererComponent (JTable table,
	                                                Object value,
	                                                boolean isSelected,
	                                                boolean hasFocus,
	                                                int row,
	                                                int column )  {
		lines = 1;
		rowWidth = table.getCellRect(row, column, false).width;
	    splitInLines(value.toString());  // Value kann String mit \n enthalten
	    int prferedHeight = (int) getPreferredSize().getHeight();
	    if (table.getRowHeight() < prferedHeight)
	    	table.setRowHeight(row, (int) getPreferredSize().getHeight());
	    return this;
	  }
	
	
	
	public void splitInLines (String string)
	{
		String out = "";
		
		int col = rowWidth;
		
		
		
		int posWhitespace = 0;
		
		int firstToCopy = 0;
		
		String line = "";
		
		
		for (int i = 0; i< string.length(); i++)
		{
			line += string.substring(i, (i + 1));
			if(Character.isWhitespace(string.charAt(i)))
				posWhitespace = i;
			if (SwingUtilities.computeStringWidth(getFontMetrics(font), line) >= col)
			{
				if(posWhitespace != 0)
				{
					out += string.substring(firstToCopy, posWhitespace) + "\n";
					firstToCopy = posWhitespace + 1;
					posWhitespace = 0;
					if (firstToCopy < i)
						line = string.substring((firstToCopy -1), i);
					else
						line = "";
					lines++;
				}
				else
				{
					out += string.substring(firstToCopy, i) + "\n";
					firstToCopy = i + 1;
					line = "";
					lines++;
				}
			}
			
		}
		
		out += string.substring(firstToCopy, string.length());
		
		
		/*for (int i = 0; i < string.length(); i++)
		{
			if (Character.isWhitespace(string.charAt(i)))
			{
				posWhitespace = i;
			}
			if(i == colTmp)
			{
				if(posWhitespace != 0)
				{
					out += string.subSequence(firstToCopy, posWhitespace) + "\n";
					firstToCopy = posWhitespace + 1;
					posWhitespace = 0;
					lines++;
				}
				else
				{
					out += string.subSequence(firstToCopy, i) + "\n";
					firstToCopy = i + 1;
					lines++;
				}
				
				colTmp += col;
			}
		}*/
		
		
		
		
		setText(out);
		
		
		
	}
	
}

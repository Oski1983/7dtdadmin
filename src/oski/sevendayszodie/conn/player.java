/**
 * 
 */
package oski.sevendayszodie.conn;

import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Field;
import java.util.logging.Logger;


/**
 * @author Oski
 *
 */
public class player {

	private final List<playerInventaryAvailable> playerInventaryAvailabelList = new ArrayList<>();

	public Logger log;
	
	public String name = null;
	public String uid = null;
	public Integer kills = 0;
	public Boolean isOnline = false;
	public Boolean isBloody = false;
	public Integer serverid = 0;
	public String lastseen = "--";
	public Integer playtime = 0;
	public String ip = "0.0.0.0";
	public Double[] pos = {
			0.0,
			0.0,
			0.0
	};
	public Double[] rot = {
			0.0,
			0.0,
			0.0
	};
	public Integer health = null;
	public Integer deaths = null;
	public Integer zombies = null;
	public Integer players = null;
	public Integer score = null;
	public Integer level = null;
	public Integer ping = null;
	public String[][] bagPack = new String[32][3];
	public String[][] belt = new String[8][3];
	public String[][] equpment = new String [11][2];
	
	
	
	
	
	/**
	 * @param args
	 */
	public player(String name, String uid, String serverid, String online, String lastseen, String playtime, String ip) {
		// TODO Auto-generated method stub
		this.name = name;
		this.uid = uid;
		this.serverid = new Integer(serverid);
		this.isOnline = new Boolean(online);
		this.lastseen = lastseen;
		this.playtime = new Integer(playtime);
		this.ip = ip;
		

	}
	
	public Object get(String var)
	{
		Class<player> cClass = player.class;
		Field field;
		try {
			field = cClass.getField(var);

			try {
				return field.get(this);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void firerPlayerInventaryAvailaible(player pl)
	{
		for (int i = 0; i < playerInventaryAvailabelList.size(); i++)
		{
			try {
				playerInventaryAvailabelList.get(i).onPlayerInventaryAvailable(pl);
			} catch (IndexOutOfBoundsException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void set(String var, Object value) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException
	{
		Class<player> cClass = player.class;
		Field field;
		field = cClass.getField(var);
		field.set(this, value);
		
	}
	
	
	
	public List<playerInventaryAvailable> getAllMessageHandlers() {
        return playerInventaryAvailabelList;
    }
	
	 public boolean addMessageHandler(playerInventaryAvailable handler) {
	        return playerInventaryAvailabelList.add(handler);
	    }

	 public boolean removeMessageHandler(playerInventaryAvailable handler) {
		
		 
	        return playerInventaryAvailabelList.remove(handler);
	    
	 }
	

}

package oski.sevendayszodie.conn;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;



public class playerTable extends AbstractTableModel {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String[] columnNames  = {
			"Name",
			"ServerId",
			"SteamUid",
			"letzte IP",
			"Zuletzt Online",
			"Spielzeit",
			"Status"
	};
	
	private String[] columnIndex = {
			"name",
			"serverid",
			"uid",
			"ip",
			"lastseen",
			"playtime",
			"isOnline"
	};
	
	public List<player> data = new ArrayList<player>();

	public playerTable() {
		// TODO Auto-generated constructor stub
		
		
	}
	
	@Override
	public String getColumnName(int col)
	{
		return this.columnNames[col];
	}

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	
	}

	@Override
	public int getRowCount() {
		if (this.data == null) return 0;
			
		return this.data.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		player pl = this.data.get(rowIndex);
		Object r = pl.get(this.columnIndex[columnIndex]);
		
		if(r instanceof String)
			return pl.get(this.columnIndex[columnIndex]);
		else if(r instanceof Boolean)
			return (this.columnIndex[columnIndex] == "isBloody") ? 
					(((Boolean)pl.get(this.columnIndex[columnIndex])) ? "Yes" : "No"):
						(((Boolean)pl.get(this.columnIndex[columnIndex])) ? "online" : "offline");
		
		else return r;
		
		/*if(columnIndex != 2 && columnIndex < 5)
		{
			player pl = this.data.get(rowIndex);
			return pl.get(this.columnIndex[columnIndex]);
		}
		else if (columnIndex < 5)
		{
			player pl = this.data.get(rowIndex);
			Team t = (Team) pl.get(this.columnIndex[columnIndex]);
			if (t != null) return t.name;
			else return defaultTeam.name;
		}
		else
		{
			player pl = this.data.get(rowIndex);
			return ((Boolean)pl.get(this.columnIndex[columnIndex])) ? "Online" : "Oflline";
		}*/
	}
	
	
	public boolean addRow(String name, String uid, String serverid, String online, String lastseen, String playtime, String ip) {
		player pl = new player(name, uid, serverid, online, lastseen, playtime, ip);
		this.data.add(pl);
		fireTableDataChanged();
		return true;
		
		
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		if(columnIndex != 2)
		{
			player pl = this.data.get(rowIndex);
			try {
				pl.set(this.columnIndex[columnIndex], aValue);
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	
	
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Class getColumnClass(int c)
	{
		if (c == 2 || c == 6) return String.class;
		
		Class<player> cClass = player.class;
		Field field = null;
		try {
			field = cClass.getField(this.columnIndex[c]);
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Class r = field.getType();
		
		
		/*Class r = new String().getClass();
		switch (c) {
		case 0:
			r = new String().getClass();
			break;
			
		case 1:
			r = new Long(1).getClass();
			break;
			
		case 2:
			r = new Integer(1).getClass();
			break;
			
		case 3:
			r = new Boolean(false).getClass();

		default:
			break;
			
		}*/
		return r;
	}
	
	public boolean removeRow(int row) {
		this.data.remove(row);
		
		return true;
	}

}

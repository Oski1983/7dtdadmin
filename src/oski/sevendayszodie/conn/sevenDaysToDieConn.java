package oski.sevendayszodie.conn;

import java.io.IOException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class sevenDaysToDieConn {
	
	static telnet tn = null;
	static playerTable plOnlineTb = null;
	static String fps = "0";
	static String time = "--";
	static boolean loggedIn;
	static String mg = null;
	static boolean isReady = false;
	static String Servername = null;
	static String MaxPlayers = null;
	static String Gamemode = null;
	static ChatTable chatTable = null;
	static String nextPackage = null;
	Thread proofThread;
	

	public sevenDaysToDieConn() {
		// TODO Auto-generated constructor stub
		
		plOnlineTb = new playerTable();
		chatTable = new ChatTable();
		
		
		
	}
	
	public boolean connect(String host, Integer port)
	{
		
		try {
			tn = new telnet(host, port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		tn.addMessageHandler(messageListener);
		
		
		
		return(tn.isConnected());
	}
	
	public boolean isConnected()
	{
		return tn.isConnected();
	}
	
	
	public synchronized boolean login(String password)
	{
		if (mg == null)
			getNextPackage();
		
		System.out.println("mg = \"" + mg + "\"");
		
		if(Pattern.compile(".*Please enter password:.*", Pattern.DOTALL).matcher(mg).matches())
		{
			tn.sendData(password);
			
			String np = getNextPackage();

			System.out.println("np = \"" + np + "\"");
			if (Pattern.compile(".*Password incorrect.*", Pattern.DOTALL).matcher(np).matches()) return false;
			
			//np = getNextPackage();
            
			System.out.println(mg);
		
			if (Pattern.compile(".*Logon successful.*", Pattern.DOTALL).matcher(mg).matches())
			{
				System.out.print("Login succsefull");
				loggedIn = true;
				/* 
				 * ToDo Hier sollten noch die globalen Serverdaten abgefragt werden aus dem erhaltenen Paket
				 */
				tn.sendData("lkp");
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	
	public MessageHandler messageListener = new MessageHandler() {
		
		@Override
		public synchronized void onMessageReceived(String message) {
			
			if (mg == null)
				mg = message;
			else if(!loggedIn || Servername == null || MaxPlayers == null || Gamemode == null)
				mg += message;
			
			nextPackage = message;
			
			isReady = true;
			this.notify();
			
			System.out.print("search String for matches\n");
			
			proofSubmittedString(message);
			// TODO Auto-generated method stub
		}
					
		
	};
	
	
	public void proofSubmittedString(String messageString)
	{
		
		proofThread = new Thread("proofThread" + System.currentTimeMillis()) {
			@Override
			public void run()
			{
				seekData(messageString);
				
			}
			public void seekData(String message) {
				Matcher matcher = Pattern.compile(".*FPS:.(?<fps>\\d{1,3}\\.\\d{1,2}).Heap.*", Pattern.MULTILINE)
						.matcher(message);
				if (matcher.find()) {
					fps = matcher.group("fps");
				}
				matcher = Pattern.compile("^.?(?<day>Day.\\d{1,3},.\\d{2}:\\d{2}).*$", Pattern.MULTILINE)
						.matcher(message);
				if (matcher.find()) {
					time = matcher.group("day");
				}
				matcher = Pattern.compile(
						"^\\d{1,2}.{2}(?<username>.*),.id=(?<serverid>\\d{1,6}),.steamid=(?<steamid>\\d{17}),.online=(?<online>False|True),.ip=(?<ip>\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}),.playtime=(?<playtime>\\d*).m,.seen=(?<lastseen>(\\d{4})-(\\d{2})-(\\d{2}).(\\d{2}):(\\d{2}))$",
						Pattern.MULTILINE).matcher(message);
				while (matcher.find()) {
					System.out.print("Zeile gefunden");
					Boolean playerFound = false;
					for (player pl : plOnlineTb.data) {
						System.out.println("Es wird nach einem vorhanden Eintrag gesucht");
						if (matcher.group("steamid").equals(pl.uid)) {
							System.out.print("es wurde ein Eintrag upgedatet");
							pl.name = matcher.group("username");
							//pl.serverid = new Integer(matcher.group("serverid"));
							pl.isOnline = new Boolean(matcher.group("online"));
							pl.playtime = new Integer(matcher.group("playtime"));
							pl.lastseen = matcher.group("lastseen");
							pl.ip = matcher.group("ip");

							playerFound = true;
						}
					}
					if (!playerFound) {
						System.out.print("es wurde ein Eintrag hinzugefügt");
						plOnlineTb.addRow(matcher.group("username"), matcher.group("steamid"),
								matcher.group("serverid"), matcher.group("online"), matcher.group("lastseen"),
								matcher.group("playtime"), matcher.group("ip"));
					}
					plOnlineTb.fireTableDataChanged();
				}
				matcher = Pattern
						.compile(
								"^\\d\\. id=(?<id>\\d{1,6}), (?<name>.*), pos=\\((?<posX>-?\\d{1,6}\\.\\d), (?<posY>-?\\d{1,6}\\.\\d), (?<posZ>-?\\d{1,6}\\.\\d)\\), rot=\\((?<rotX>-?\\d{1,6}\\.\\d), (?<rotY>-?\\d{1,6}\\.\\d), (?<rotZ>-?\\d{1,6}\\.\\d)\\), remote=(?<remote>(True|False)), health=(?<health>\\d{1,4}), deaths=(?<deaths>\\d*), zombies=(?<zombies>\\d*), players=(?<players>\\d*), score=(?<score>\\d*), level=(?<level>\\d*), steamid=(?<steamId>\\d{17}),.ip=(?<ip>\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}), ping=(?<ping>\\d{1,4})$")
						.matcher(message);
				while (matcher.find()) {
					//Find Player by his SteamUID or add a new one if not found

					for (player pl : plOnlineTb.data) {

						if (pl.uid.equals(matcher.group("steamId"))) {
							pl.name = matcher.group("name");
							pl.pos = new Double[] { new Double(matcher.group("posX")),
									new Double(matcher.group("posY")), new Double(matcher.group("posZ")) };
							pl.rot = new Double[] { new Double(matcher.group("rotX")),
									new Double(matcher.group("rotY")), new Double(matcher.group("rotZ")) };
							pl.health = new Integer(matcher.group("health"));
							pl.deaths = new Integer(matcher.group("deaths"));
							pl.zombies = new Integer(matcher.group("zombies"));
							pl.players = new Integer(matcher.group("players"));
							pl.score = new Integer(matcher.group("score"));
							pl.level = new Integer(matcher.group("level"));
							pl.ip = matcher.group("ip");
							pl.ping = new Integer(matcher.group("ping"));
							System.out.println(pl);
							plOnlineTb.fireTableDataChanged();
							return;
						}

					}

					tn.sendData("lkp");

				}
				matcher = Pattern.compile(
						"(?m)^(?<year>\\d{4})-(?<month>\\d{2})-(?<day>\\d{2})T(?<hour>\\d{2}):(?<minute>\\d{2}):(?<second>\\d{2}) \\d*\\.\\d{3} INF GMSG: (?<sender>.*): (?<message>.*)$",
						Pattern.MULTILINE).matcher(message);
				if (matcher.find()) {
					@SuppressWarnings("deprecation")
					Date date = new Date((new Integer(matcher.group("year")) - 1900),
							(new Integer(matcher.group("month")) - 1), new Integer(matcher.group("day")),
							new Integer(matcher.group("hour")), new Integer(matcher.group("minute")),
							new Integer(matcher.group("second")));

					java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd.MM.yyy HH:mm:ss");

					chatTable.addRow(formatter.format(date), matcher.group("sender"), matcher.group("message"));
				}
				if (MaxPlayers == null && mg != null) {
					matcher = Pattern.compile("^Max.players:.(?<maxPlayers>\\d{1,3})$", Pattern.MULTILINE)
							.matcher(mg);
					if (matcher.find())
						MaxPlayers = matcher.group("maxPlayers");
				}
				if (Gamemode == null && mg != null) {
					matcher = Pattern.compile("^Game.mode:.{3}(?<gameMode>.*)$", Pattern.MULTILINE).matcher(mg);
					if (matcher.find())
						Gamemode = matcher.group("gameMode");
				}
				if (Servername == null && mg != null) {
					matcher = Pattern.compile("^Game.name:.{3}(?<gamename>.*)$", Pattern.MULTILINE).matcher(mg);
					if (matcher.find())
						Servername = matcher.group("gamename");
				}
			}
			
			
			
		};
		
		proofThread.start();
	}
	
	
	
	public String getNextPackage()
	{
		isReady = false;
		synchronized (messageListener) {
			while(!isReady)
				try {
					messageListener.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		return nextPackage;
		
		
	}
	
	public void getInventary(player pl)
	{
		Thread tr = new Thread("getInventarOf" + pl.name)
				{
			public void run()
			{
				sendData("si " + pl.uid);
				registerMessageHandler(new MessageHandler() {
					String packet = "";
					Boolean packetStart = false;
					Boolean lastPacket = false;
					Boolean packetEnd = false;
					@Override
					public void onMessageReceived(String message) {
						// TODO Auto-generated method stub
						if(Pattern.compile("^Belt.of.player." + Pattern.quote(pl.name) + ":.?$", Pattern.DOTALL).matcher(message).matches())
							packetStart = true;
						if (packetStart && !packetEnd)
						{
							packet += message;
							if (lastPacket && !Pattern.compile(".*Slot.*", Pattern.DOTALL).matcher(message).matches())
							{
								packetEnd = true;
							}
							if (Pattern.compile("^Equipment.of.player." + Pattern.quote(pl.name) + ":.?$", Pattern.DOTALL).matcher(message).matches())
								lastPacket = true;
							
							
						}
						else if (packetEnd)
						{
							seekData(packet);
							unregisterMessageHandler(this);
						}
						
					}
				});
				
				
				
			}
			
			public void seekData(String packet)
			{
				Boolean belt = false;
				Boolean bagpack = false;
				
				pl.bagPack = new String[32][3];
				pl.belt = new String[8][3];
				
				String[] lines = packet.split("\n");
				
				for(String line : lines)
				{
					System.out.println(line);
					if (Pattern.compile("Bagpack of player "+Pattern.quote(pl.name)+":", Pattern.DOTALL).matcher(line).matches())
						belt = true;
					if (belt && Pattern.compile("Equipment of player "+Pattern.quote(pl.name)+":", Pattern.DOTALL).matcher(line).matches())
						bagpack = true;
					
					Matcher matcher = Pattern.compile(".*Slot (?<slot>\\d{1,2}): (?<count>\\d{1,3}) \\* (?<name>[A-z0-9]*)( - quality: (?<quality>\\d{1,3}))?", Pattern.DOTALL).matcher(line);
					if (matcher.find())
					{
						System.out.println(matcher.group());
						if (!belt && !bagpack)
						{
							Integer index = new Integer(matcher.group("slot"));
							pl.belt[index][0] = matcher.group("name");
							pl.belt[index][1] = matcher.group("count");
							pl.belt[index][2] = matcher.group("quality");
							
						}
						if (belt && !bagpack)
						{
							Integer index = new Integer(matcher.group("slot"));
							pl.bagPack[index][0] = matcher.group("name");
							pl.bagPack[index][1] = matcher.group("count");
							pl.bagPack[index][2] = matcher.group("quality");
							
							
						}
						
					}
					matcher = Pattern.compile(".*Slot (?<slot>[a-z]{4,8}): (?<name>.[A-z0-9]*) - quality: (?<quality>\\d{1,3})", Pattern.DOTALL).matcher(line);
					if(matcher.find())
					{
						System.out.println(matcher.group("name") + "gefunden");
						Integer index;
						switch (matcher.group("slot")) {
						case "head":
							index = 0;
							break;
							
						case "eyes":
							index = 1;
							break;
							
						case "face":
							index = 2;
							break;
							
						case "armor":
							index = 3;
							break;
						
						case "jacket":
							index = 4;
							break;
							
						case "shirt":
							index = 5;
							break;
							
						case "legarmor":
							index = 6;
							break;
							
						case "pants":
							index = 7;
							break;
							
						case "boots":
							index = 8;
							break;
						
						case "gloves":
							index = 9;
							break;

						default:
							index = 0;
							break;
							
							
						}
						
						pl.equpment[index][0] = matcher.group("name");
						pl.equpment[index][1] = matcher.group("quality");
					}
	
				}
				pl.firerPlayerInventaryAvailaible(pl);
				
				
			}
			
			
			
				};
				
				
				tr.start();
	}
	
	
	public void registerMessageHandler(MessageHandler mg)
	{
		tn.addMessageHandler(mg);
	}
	
	public void unregisterMessageHandler(MessageHandler mg)
	{
		tn.removeMessageHandler(mg);
	}
	
	
	public playerTable getPlayerAllTable()
	{
		return plOnlineTb;
	}

	
	public String getFps()
	{
		return fps;
	}
	
	public void close()
	{
		tn.close();
	}
	
	public void sendData(String data)
	{
		tn.sendData(data);
	}
	
	public String getTime()
	{
		tn.sendData("gt");
		return time;
	}
	
	public String getGameName()
	{
		return(Servername);
	}
	
	public String getMaxPlayers()
	{
		return(MaxPlayers);
	}
	
	public String getGameMode()
	{
		return (Gamemode);
	}
	
	public ChatTable getChatTable()
	{
		return (chatTable);
	}
	

	

}


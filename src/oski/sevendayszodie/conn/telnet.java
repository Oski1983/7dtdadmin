package oski.sevendayszodie.conn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.net.telnet.TelnetClient;


public class telnet {
	
	public TelnetClient tc = null;
	byte[] buff = new byte[1024];
	private OutputStream outstr;
	public boolean loggedIn = false;
	BufferedReader br = null;
	static pauseableThread reader;
	
    private final List<MessageHandler> messageHandlerList;
	
	public telnet(String host, int port) throws SocketException, IOException
	{
		messageHandlerList = new ArrayList<>();
		
		
		tc = new TelnetClient();
		
		
			
				tc.connect(host, port);
				tc.setKeepAlive(true);
				System.out.print("verbunden mit " +  host + "\n");
				
				System.out.println(tc.getCharsetName());

				outstr = tc.getOutputStream();
			
			
			 reader = new pauseableThread() {
				 
				 
				@Override
				public void run()
				{
					
					
					
					InputStream instr = tc.getInputStream();
					try {
						br = new BufferedReader(new InputStreamReader(tc.getInputStream(), "UTF-8"));
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					

			        	String line = null;
			        	try {
							while((line = br.readLine()) != null)
							{
								synchronized(this){
									while(fPause)
									{
										try {
											System.out.println("Thread wird pausiert");
											wait();
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
								}
								System.out.println("Thread wird ausgeführt");
								fireMessageHandler(line + "\n");
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			            /*byte[] buff = new byte[1024];
			            int ret_read = 0;

			            do
			            {
			                ret_read = instr.read(buff);
			                if(ret_read > 0)
			                {
			                	//String line = new String(buff, 0, ret_read);
			                	//fireMessageHandler(line);
			                    //System.out.print(new String(buff, 0, ret_read));
			                	readLine();
			                }
			            }
			            while (ret_read >= 0);*/
			        }
				
				
				
				
				
			};
			
			
			reader.start();
			
			
		
		
			
		
	}
	
	
	
	
	
	public void close()
	{
		if (this.tc != null)
			try {
				this.tc.disconnect();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public void sendData(String Data)
	{
		Data = Data + "\n";
		
		
		
		
		try {
			System.out.print("sende Daten " + Data + "\n");
			this.outstr.write(Data.getBytes("UTF-8"));
			this.outstr.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean isConnected()
	{
		return tc.isConnected();
	}
	
	


	public List<MessageHandler> getAllMessageHandlers() {
        return messageHandlerList;
    }
	
	 public boolean addMessageHandler(MessageHandler handler) {
	        return messageHandlerList.add(handler);
	    }

	 public boolean removeMessageHandler(MessageHandler handler) {
		
		 synchronized (reader) {
			reader.pause();
		}
	        Boolean r = messageHandlerList.remove(handler);
	     synchronized (reader) {
			reader.play();
		}   
	        return r;
	 }
	
	
	public void fireMessageHandler(String message) {
        if (message == null || message.isEmpty()) {
            return;
        }
        for (int i = 0; i < messageHandlerList.size(); i++) {
        	try {
				if(messageHandlerList.get(i) != null)
					messageHandlerList.get(i).onMessageReceived(message);
			} catch (IndexOutOfBoundsException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }



}	


class pauseableThread extends Thread {
	 boolean fPause = false;
	 
	 
	 
		
		
		public void pause()
		{
			this.fPause = true;
		}
		
		public void play()
		{
			this.fPause = false;
			notifyAll();
		}
		
		
		

}

